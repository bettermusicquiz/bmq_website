@echo off
REM This file allows you to use the command "./be" as a pseudo-alias for the "bundle exec" command.
REM
REM If you want to execute this command by typing simply "be", edit your Windows PowerShell profile and add the following line:
REM     Set-Alias be "./be"
REM To find the location of your profile, run the following:
REM     $Profile
REM You will probably have to create this folder and file, then populate it with the line above.
REM
REM Naturally, this alias will only work correctly when you are in the same folder as this file.
REM
bundle exec %*
