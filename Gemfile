source 'https://rubygems.org'

# NOTE: because Heroku throws out our Windows Gemfile.lock and generates its own,
# we should specify version ranges to ensure Heroku doesn't get a totally different gem version.

# Needed by Heroku.
# See https://devcenter.heroku.com/articles/ruby-versions for more information.
ruby '2.4.3'

# Use Rails. Lock to verson 4.2.10 so it doesn't suddenly change on the Heroku server.
gem 'rails', '4.2.10'

# Use postgresql as the database for Active Record.
# Rails 4.2.10 does not support pg 1.0.0 or above.
gem 'pg', '~> 0.21.0'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0', '>= 5.0.7'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Sprockets is a Rack-based asset packaging system that concatenates and serves
# JavaScript, CoffeeScript, CSS, LESS, Sass, and SCSS.
gem 'sprockets', '~> 3.7', '>= 3.7.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 1.0', group: :doc

# Use Puma as the app server.
gem 'puma', '~> 3.11', '>= 3.11.2'

# Use Rack Timeout to raise an error when a request takes too long.
gem 'rack-timeout', '~> 0.4.2'

# Nokogiri is an HTML, XML, SAX, and Reader parser.
gem 'nokogiri', '~> 1.8', '>= 1.8.2'

# Use Devise for member authentication.
# Note: on Windows, you may need to uninstall and reinstall bcrypt manually by running the following:
#   gem uninstall bcrypt
#   gem install bcrypt --platform=ruby
gem 'devise', '~> 3.5', '>= 3.5.10'

# Use CanCanCan (continuation of CanCan) for authorization/permissions.
gem 'cancancan', '~> 2.1', '>= 2.1.3'

# Use Postmark to send transactional emails.
gem 'postmark-rails', '~> 0.15.0'

# Use Haml syntax for HTML pages, but process it using Hamlit for better performance.
gem 'haml-rails', '~> 1.0'
gem 'hamlit', '~> 2.8', '>= 2.8.7'

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Use jQuery UI alongside jQuery.
gem 'jquery-ui-rails', '~> 6.0', '>= 6.0.1'

# Add helper functions (and improved functionality) for jQuery UI autocomplete fields.
gem 'rails-jquery-autocomplete', '~> 1.0', '>= 1.0.3'

# Use Jade javascript templates.
# Use Pug once the gems work without requiring Pug to be installed separately.
gem 'jade-rails', '~> 1.11', '>= 1.11.0.1'

# Use ZURB Foundation for responsive CSS classes.
gem 'foundation-rails', '~> 5.5', '>= 5.5.3.2'

# Use Font Awesome for icons.
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.3'

# Use rails-assets.org to install Bower packages as Rails gems.
# Example usage:
#   gem 'rails-assets-BOWER_PACKAGE_NAME'
source 'https://rails-assets.org' do
  # Use Underscore.js javascript helper methods.
  gem 'rails-assets-underscore', '~> 1.8.3'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  #gem 'byebug', '9.0.6'

  # Use Dotenv to load environment variables from the flie `.env`.
  gem 'dotenv-rails', '~> 2.2', '>= 2.2.1'
end

group :development do
  # Suppress log messages for the loading of assets.
  gem 'quiet_assets', '~> 1.1.0'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.3'
end

group :production do
  # Needed by Heroku.
  gem 'rails_12factor'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
