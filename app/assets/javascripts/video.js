$(document).ready(function() {
  (function(Video) {
    // ----------------------------------------------------------------------------------------------------
    // Private constants.


    var JST_PATHS = {
      youtubeVideo: "templates/video/youtube"
    };


    // ----------------------------------------------------------------------------------------------------
    // Public functions.


    Video.embedYouTubeVideo = function(videoId, $elem) {
      jstContext = {
        videoId: videoId,
      };
      $elem.html(JST[JST_PATHS.youtubeVideo](jstContext));
    };


  })(window.Video = window.Video || {});
});
