# Terminate requests that continue for this many seconds.
# Will output a stack trace to the logs.
Rack::Timeout.timeout = 20.seconds

if Rails.env.development?
  # Disable rack timeout's unneeded log messages.
  Rack::Timeout::Logger.disable
end
